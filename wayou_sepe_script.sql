-- Afficher les patients nés à partir du premier janvier 
select * from patient where date_naissance >= '1990-01-01' order by date_naissance ;

-- Afficher les patients nés à partir du premier janvier et de sexe feminin
select * from patient where date_naissance >= '1990-01-01' and sexe = 'F' order by date_naissance ;


-- Afficher les patients nés à partir du premier janvier, de sexe feminin et résidant dans les villes id 1,2,3
select * from patient where date_naissance >= '1990-01-01' and sexe = 'F' and id_ville in (1,3,5)  order by date_naissance ;

--Afficher les patients résidant dans les villes Lille et Roubaix. Pour cette requête, ne pas utiliser
--directement les identifiants des villes mais plutôt leurs noms (Lille et Roubaix)

select * from (select * from patient p, ville v where v.id_ville = p.id_ville) as la_table where ville = 'Roubaix' or ville = 'Lille';

--Compter le nombre de patients résidant dans les villes Lille et Roubaix (pour chacune des villes).
--Pour cette requête, ne pas utiliser directement les identifiants des villes mais plutôt leurs noms (Lille
--et Roubaix)

select count(id_patient) as nombre_patient, ville from (select * from patient p, ville v where v.id_ville = p.id_ville) as la_table where ville = 'Roubaix' or ville = 'Lille' 
group by ville ;

--Compter le nombre de séjours débutant chaque jour, en les triant par date de début.
select count(*) as number_d, date_debut_sejour  from sejour  group by date_debut_sejour  order by date_debut_sejour ; 

-- Compter le nombre de séjours débutant chaque jour, en les triant par date de début et en ne
-- conservant que les jours ayant plus de 2 séjours.


select * from(select count(*) as number_d, date_debut_sejour  from sejour  group by date_debut_sejour order by date_debut_sejour) as ma_table where number_d > 2 ; 

--Afficher les RUMs du patient n°1
select * from rum; 
select * from diagnostic ;
select * from rum_acte;
select * from acte ; 
select * from ville ;
select * from patient ;
select * from hopital; 
select * from sejour ; 
select * from rum_diagnostic ;




-- Afficher les RUMs du patient n°1
select id_patient, id_rum from (
select * from ( 
select T.id_patient, T.id_sejour, T.id_rum, ra.id_acte, ra.date_acte from (select id_patient, id_sejour, id_rum from 
(select S.id_sejour,  S.id_patient, R.id_rum  from sejour S inner join rum R on R.id_sejour = S.id_sejour) as une_table) as T  
inner join rum_acte ra  on ra.id_rum = T.id_rum)  as D inner join acte A on D.id_acte = A.id_acte ) as B where B.id_patient  = 1 ; 


--Insérer 3 RUMs pour le séjour n°11. Vous pouvez choisir les dates, modes d'admission et mode de
--sortie. Le tout doit être cohérent.

insert into  rum (id_rum, id_sejour, date_entree, date_sortie, mode_admission, mode_sortie) 
values (6, 11, '2020-01-11', '2020-01-13', 9, 9),  
 (7, 11, '2020-01-13', '2020-01-15', 9,10),  
 (8, 11, '2020-01-16', '2020-01-18', 8,8)
;


--Insérer 1 acte pour chacun des RUMs du séjours n°10
insert into rum(id_rum, id_sejour, date_entree, date_sortie, mode_admission, mode_sortie)
values(9, 10, '2020-01-02', '2020-01-03', 9, 9),
(10,10, '2020-01-03', '2020-01-04', 10, 10);

--il faut inserer dans la table acte 2 actes dans la table actes 
-- il faut insérer deux actes dans la table rum_acte avec id_acte et id_rum correspondant aux valeurs qui sont dans la rum et acte.
insert into acte (id_acte, code_acte, libelle_acte, date_debut_validite, date_fin_validite)
values(6, 'ACT6000', 'Acte 6000', '2010-01-01', '2020-12-31'), 
(7, 'ACT7000', 'Acte 7000', '2020-01-01', '2020-12-31') ; 

insert into rum_acte(id_acte, id_rum, date_acte) values
(6, 9, '2020-01-03' ), 
(7, 10, '2020-01-04')



-- Insérer 1 diagnostic pour chacun des RUMs du séjours n°10
insert into diagnostic (id_diagnostic, code_diagnostic, libelle_diagnostic, date_debut_validite, date_fin_validite) 
values (6, 'D600', 'Diagnostic 600', '2010-01-01', '2020-12-31'),  
 (7, 'D700', 'Diagnostic 700', '2010-01-01', '2020-12-31');
insert into rum_diagnostic values 
(6,9, '2020-08-01'), 
(7, 10, '2020-07-12')


---créer une table medicament permettant de stocker une liste de médicaments 
create table MEDICAMENT (id_medicament int not NULL, nom_medicament varchar(256),  
constraint pk_medicament primary key (id_medicament)) ;

--- Créer une table RUM_MEDICAMENT permettant de stocker des administrations de médicaments
--- lors de RUM.
create table RUM_MEDICAMENT(
id_rum_medicament int not null,  
id_rum int not null, 
id_medicament int not null,  
constraint pk_rm_med primary key (id_rum_medicament, id_rum, id_medicament),
constraint fk_rm foreign key (id_rum) references rum (id_rum),  
constraint fk_rr foreign key (id_medicament) references MEDICAMENT (id_medicament))
--les date de rum doivent être comprise entre les date_debut_sejour et date_fin_sejour 

-- pour le rum cree un sejour 10 dans la table rum ensuite je cree un acte dans la table acte pour le sejour.
insert into MEDICAMENT (id_medicament, nom_medicament)
values (1, 'ASPIRINE'),  
(2, 'FERVEX'),  
(3, 'DAFALGAN'),  
(4, 'PULMOSERUM'),  
(5, 'DOLIRANE '),  
(6, 'PRORHINEL')
-- Insérer 5 administrations de médicaments dans la table RUM_MEDICAMENT, pour le séjour 10.
insert into  RUM_MEDICAMENT(id_rum_medicament, id_rum, id_medicament )
values (1, 10,  2),  
(2, 10,3), 
(3, 10, 4), 
(4, 10, 1), 
(5, 10, 6)

select * from RUM_MEDICAMENT ;